using System;

namespace DistanceTask
{
    public class DistanceSolution
    {
        static double CalculateDistanceBetweenPoints(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
        }

        // ���������� �� ����� (x, y) �� ������� AB � ������������ A(aX, aY), B(bX, bY)
        public static double GetDistanceToSegment(double aX, double aY, double bX, double bY, double x, double y)
        {
            
            var lineIsSinglePoint = aX == bX && aY == bY;
            var aToX = CalculateDistanceBetweenPoints(aX, aY, x, y);
            if (lineIsSinglePoint) return aToX;
            var bToX = CalculateDistanceBetweenPoints(bX, bY, x, y);
            var lineSize = CalculateDistanceBetweenPoints(bX, bY, aX, aY);
            // ���������� ����� ������..
            // Ax + By + C = 0
            var a = aY - bY;
            var b = bX - aX;
            var c = aX * bY - bX * aY;
            var h = Math.Abs(a * x + b * y + c) / Math.Sqrt(a * a + b * b);
            var hIsOutOfLine = Math.Max(Math.Sqrt(aToX * aToX - h * h), Math.Sqrt(bToX * bToX - h * h)) > lineSize;
            return hIsOutOfLine ? Math.Min(aToX, bToX) : h;
        }
    }
}