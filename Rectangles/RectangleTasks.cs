﻿using System;
using System.Drawing;

namespace Rectangles
{
	public class RectangleTasks
	{

        private static int YAxisIntersection(Rectangle r1, Rectangle r2)
        {
            return r1.Y > r2.Y ?
                r1.Bottom > r2.Bottom ? r2.Bottom - r1.Y : r1.Height :
                r2.Bottom > r1.Bottom ? r1.Bottom - r2.Y : r2.Height;
        }

        private static int XAxisIntersection(Rectangle r1, Rectangle r2)
        {
            return r1.X > r2.X ?
                r1.Right > r2.Right ? r2.Right - r1.X : r1.Width :
                r2.Right > r1.Right ? r1.Right - r2.X : r2.Width;
        }

		// Пересекаются ли два прямоугольника (пересечение только по границе также считается пересечением)
		public bool AreIntersected(Rectangle r1, Rectangle r2)
		{
            var yIntersection = YAxisIntersection(r1, r2);
            var xIntersection = XAxisIntersection(r1, r2);
            return yIntersection >= 0 && xIntersection >= 0;
		} 

		// Площадь пересечения прямоугольников
		public int IntersectionSquare(Rectangle r1, Rectangle r2)
		{
            var yIntersection = YAxisIntersection(r1, r2);
            var xIntersection = XAxisIntersection(r1, r2);
            if (xIntersection <= 0 || yIntersection <= 0) return 0;
            return xIntersection*yIntersection;
		}

		// Если один из прямоугольников целиком находится внутри другого — вернуть номер (с нуля) внутреннего.
		// Иначе вернуть -1
		public int IndexOfInnerRectangle(Rectangle r1, Rectangle r2)
		{
            var yIntersection = YAxisIntersection(r1, r2);
            var xIntersection = XAxisIntersection(r1, r2);
			if (r1.Width <= r2.Width && r1.Height <= r2.Height) {
				return r1.Width != xIntersection || r1.Height != yIntersection ? -1 : 0;
			}
			if (r2.Width <= r1.Width && r2.Height <= r1.Height) {
				return r2.Width != xIntersection || r2.Height != yIntersection ? -1 : 1;
			}
			return -1;
		}
	}
}