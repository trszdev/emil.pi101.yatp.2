﻿using System;
using System.Linq;
using System.Drawing;

namespace WindowAndFridge
{
    public class Fridge 
    {
        public Fridge(double[] size) { Size = size; }
        public double[] Size { get; private set; }
    }

    public class Window
    {
        public Size Size { get; set; }
    }



    static class EntryPoint
    {
        static bool CanPassFridge(Window w, Fridge f)
        {
            var firstTry = (w.Size.Width / f.Size[0] >= 1 && w.Size.Height / f.Size[1] >= 1) ||
                (w.Size.Width / f.Size[1] >= 1 && w.Size.Height / f.Size[0] >= 1);
            var secondTry = (w.Size.Width / f.Size[1] >= 1 && w.Size.Height / f.Size[2] >= 1) ||
                (w.Size.Width / f.Size[2] >= 1 && w.Size.Height / f.Size[1] >= 1);
            var thirdTry = (w.Size.Width / f.Size[0] >= 1 && w.Size.Height / f.Size[2] >= 1) ||
                (w.Size.Width / f.Size[2] >= 1 && w.Size.Height / f.Size[0] >= 1);
            return firstTry || secondTry || thirdTry;
        }

        static void Main(string[] args)
        {
            var numbers = Console.ReadLine().Split()
                .Select(a => Convert.ToInt32(a))
                .ToArray();
            var fridge = new Fridge(new double[] {numbers[0], numbers[1], numbers[2]});
            var window = new Window { Size = new Size(numbers[3], numbers[4]) };
            Console.WriteLine(CanPassFridge(window, fridge));
            Console.ReadLine();
        }
    }
}
