﻿using System;
using System.IO;

namespace Pluralize
{
	static class Program
	{

		static void Main(string[] args)
		{
			// Это пример ввода сложных данных из файла.
			// Циклы, строки, массивы будут рассмотрены на лекциях чуть позже, но это не должно быть препятствием вашему любопытству! :)
			var lines = File.ReadAllLines("rubles.txt");
			var hasErrors = false;
			foreach (var line in lines)
			{
				var words = line.Split(' ');
				int count = int.Parse(words[0]);
				var rightAnswer = words[1];
				var pluralizedRubles = PluralizeRubles(count);
				if (pluralizedRubles != rightAnswer)
				{
					hasErrors = true;
					Console.WriteLine("Wrong answer: {0} {1}", count, pluralizedRubles);
				}
			}
			if (!hasErrors)
				Console.WriteLine("Correct!");
            Console.ReadLine();
		}

		private static string PluralizeRubles(int count)
		{
            const string defaultString = "рублей";
            if (count % 100 > 4 && count % 100 < 21) return defaultString;
            switch (count % 10)
            {
                case 1: return "рубль";
                case 2: case 3: case 4: return "рубля";
            }
            return defaultString;
		}
	}
}
